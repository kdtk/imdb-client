const Winston = require('winston')
const Movie = require('./models/movie');
const TvShow = require('./models/tvshow');

const IMDB = require('imdb-api');
const TMDB = require('tmdb').Tmdb;

let Mock = null;

try {
  // Mock = require('./mock.json');
  // console.info('********************');
  // console.info('***** USE MOCK *****');
  // console.info('********************');
} catch(e) {

}

const Config = process.env;

const ImdbCli = new IMDB.Client({apiKey: Config.IMDB_API_KEY});
const TmdbCli = new TMDB(Config.TMDB_API_KEY, 'it');

let TmdbImagesConfig = null;
let TmdbConfig = null;

Promise.all([TmdbCli.get('configuration'), TmdbCli.get('configuration/languages')]).then( (rsp) => {
  let config = rsp[0];
  let langs = rsp[1];
  TmdbConfig = config;
  TmdbConfig.Languages = langs.filter( l => l.name && l.englishName && l.englishName != 'No Language' ).map( l => l.iso6391 );
  TmdbImagesConfig = config.images;
  // console.info( TmdbConfig );
}).catch( (err) => {
  console.error(err);
});

let Log = Winston.createLogger({
  level: 'info',
  // format: winston.format.json(),
  // defaultMeta: { service: 'user-service' },
  transports: [new Winston.transports.File({ filename: "./imdb.log" , level: 'debug', format: Winston.format.simple() })]
});


function searchByTerms(terms) {

  // return ImdbCli.search({'name': terms}, null, {headers: { 'Accept-Language': 'it'}});
  return TmdbCli.get('search/multi', {query: terms, language: TmdbCli.language});

}


function getInfo(id, type = 'movie') {
  // return ImdbCli.get({'id': id}, {headers: { 'Accept-Language': 'it'}});

  if ( Mock && Mock[ type ] ){
    if ( type == 'movie' ) {
      return Promise.resolve( new Movie(Mock[ type ], TmdbConfig) );
    } else if ( type == 'tv' ) {
      return Promise.resolve( new TvShow(Mock[ type ], TmdbConfig) );
    } else {
      throw new Error('no supported media');
    }
  }

  return TmdbCli.get(`${type}/${id}`, {append_to_response: 'videos,images,credits', include_image_language: 'it', language: TmdbCli.language}).then( (data) => {
    let imdbid = data.imdbId;

    let p = Promise.resolve( data );

    if ( imdbid ) {
      p = ImdbCli.get({'id': imdbid}, {headers: { 'Accept-Language': 'it'}}).then( (imdb_data) => {
        data.imdb_data = imdb_data;
        return data;
      });
    } else if ( type == 'tv' ) {
      p = new Promise( (resolve, reject) => {
        ImdbCli.search({'name': data.originalName}, null, {headers: { 'Accept-Language': 'it'}}).then( (imdb_results) => {
          if ( imdb_results && imdb_results.results ) {
            // have results
            let series_results = imdb_results.results.filter( i => i.type == 'series' );
            let serie = series_results[ 0 ];
            if ( serie ) {
              data.imdbId = serie.imdbid;
              ImdbCli.get({'id': serie.imdbid}, {headers: { 'Accept-Language': 'it'}}).then( (imdb_data) => {
                data.imdb_data = imdb_data;
                resolve(data);
              }, () => {
                resolve(data);
              });
            }
          }
          resolve(data);
        }, () => {
          resolve(data);
        });
      })
    }

    return p.then( async (data) => {
      if ( type == 'movie' ) {
        return new Movie(data, TmdbConfig);
      } else if ( type == 'tv' ) {
        Log.info(`get seasons infos for ${id}`);
        data.seasons = await getInfoSeasons( id, data.seasons.map( s => s.seasonNumber ) );
        return new TvShow(data, TmdbConfig);
      } else {
        throw new Error('no supported media');
      }
    });

  });

}


async function getInfoSeasons(tv_id, numbers) {
  let seasons = [];
  Log.info(`total seasons for ${tv_id}: ${numbers.length}`);
  for ( let num of numbers ) {
    let season = await getSingleSeason( tv_id, num );
    seasons.push( season );
  }
  return seasons;
}



async function getSingleSeason(tv_id, number) {
  Log.info(`getting info for ${tv_id} season ${number}`);
  return TmdbCli.get(`tv/${tv_id}/season/${number}`, {append_to_response: 'videos,images,credits', include_image_language: 'it', language: TmdbCli.language}).then( (data) => {
    return data;
  }, (err) => {
    return err;
  });
}


module.exports = {
  Log: Log,
  searchByTerms: searchByTerms,
  getInfo: getInfo,
  getImagesOpts: function() {
    return TmdbImagesConfig;
  }
};
