import Vue from 'vue';

import video_template from "../views/components/video.pug";



function array_move(arr, old_index, new_index) {
  if (new_index >= arr.length) {
    var k = new_index - arr.length + 1;
    while (k--) {
      arr.push(undefined);
    }
  }
  arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
  return arr; // for testing
};


const VideoObject = {
  hidden: false,
  link: '',
  source: '',
  size: '',
  lang: '',
  codecAudio: '',
  channels: '',
  codecVideo: '',
  resolution: ''
};


const VideoMetaData = {
  sources: ['', 'BDRip'],
  // size: ''
  languages: ['', 'IT_EN', 'IT_BG', 'IT_CS', 'IT_DA', 'IT_DE', 'IT_EL', 'IT_ES', 'IT_ET', 'IT_JP', 'IT_FI', 'IT_FR', 'IT_GA', 'IT_HR', 'IT_HU', 'IT_LT', 'IT_LV', 'IT_MT', 'IT_NL', 'IT_PL', 'IT_PT', 'IT_RO', 'IT_SK', 'IT_SL', 'IT_SV'],
  codecsAudio: ['', 'DTS'],
  channels: ['', '5.1', '7.1'],
  codecsVideo: ['', 'x265'],
  resolutions: ['', '2160p', '1080p', '720p']
};


const VideoLinksComponent = Vue.component('VideoLinks', {

  template: video_template(),

  props: ['loadedLinks'],


  data: function() {
    return {

      video_meta_data: VideoMetaData,

      videoLinks: []

    }
  },

  created() {

    this.addVideoLink()
    this.addVideoLink()

  },

  mounted() {

  },

  computed: {

  },

  watch: {

    loadedLinks: {
      immediate: true,
      handler: function(n_value) {
        this.videoLinks.splice(0, this.videoLinks.length );
        for ( let value of n_value ) {
          let link = value.link;
          let hidden = value.hidden;
          let obj = extractVData(link);
          obj.hidden = !!hidden;
          this.videoLinks.push( obj );
        }
      }
    },

    videoLinks: {
      handler: function(n_value) {
        let res = n_value.filter(v => !!v.link.trim() ).map( (video) => {
          if ( video.link ) {
            return {hidden: video.hidden, link: this.calculateUrlVideo( video )};
          } else {
            return '';
          }
        });
        this.$parent.$emit('set-videos', {links: res});
      },
      deep: true
    }
  },



  methods: {


    calculateUrlVideo(video) {
      let audio_data = [
        video.codecAudio,
        video.lang,
        video.channels
      ].filter( d => !!d.trim() );


      let data = [
        video.source,
        video.size ? `${video.size}GB` : '',
        audio_data.join('_'),
        video.codecVideo,
        video.resolution,
      ].filter( d => !!d.trim() );

      let data_str = data.join('-');

      if ( ! video.resolution ) {
        // remove tags if no resolution found
        data_str = '';
      }

      let lv = video.link.trim();

      if ( lv.slice(-1) !== '/' && data_str) {
        lv += '/';
      }

      return `${lv}${data_str ? '[' + data_str + ']' : ''}`;
    },

    moveUp(index) {
      if ( index == 0 ) {
        return;
      }
      array_move(this.videoLinks, index, index - 1);
    },

    moveDown(index) {
      if ( index == this.videoLinks.length - 1 ) {
        return;
      }
      array_move(this.videoLinks, index, index + 1);
    },

    removeVideoLink(index) {
      this.videoLinks.splice(index, 1);
    },

    addVideoLink() {
      if ( this.videoLinks.length > 5 ) {
        return;
      }
      let data = Object.assign({}, VideoObject);
      if ( this.videoLinks.length == 0 && Card.movie ) {
        data.source = 'BDRip';
        data.resolution = '1080p';
      }
      this.videoLinks.push( data );
    },
  }

});


function extractVData(link) {
  let video_data = Object.assign({}, VideoObject);
  video_data.resolution = '';
  let matches = link.match(/\[(.*)\]$/);
  if ( matches && matches[1] ) {

    let str = matches[1];
    let splits = str.split('-');
    for ( let split of splits ) {

      let c_splits = split.split('_');
      for ( let i = 0, c_split;  c_split = c_splits[i]; i++ ) {

        if ( c_split == 'IT' ) {
          // workaround for language
          c_split = `${c_split}_${c_splits[ ++i]}`;
        }

        if ( c_split.endsWith('GB') ) {
          video_data.size = c_split.substring(0, c_split.indexOf('GB') );
        } else if ( VideoMetaData.sources.indexOf( c_split ) > -1  ) {
          video_data.source = c_split;
        } else if ( VideoMetaData.languages.indexOf( c_split ) > -1  ) {
          video_data.lang = c_split;
        } else if ( VideoMetaData.codecsAudio.indexOf( c_split ) > -1  ) {
          video_data.codecAudio = c_split;
        } else if ( VideoMetaData.channels.indexOf( c_split ) > -1  ) {
          video_data.channels = c_split;
        } else if ( VideoMetaData.codecsVideo.indexOf( c_split ) > -1  ) {
          video_data.codecVideo = c_split;
        } else if ( VideoMetaData.resolutions.indexOf( c_split ) > -1  ) {
          video_data.resolution = c_split;
        }

      }
    }

    video_data.link = link.substring(0, matches.index);

  } else {
    video_data.link = link;
  }

  return video_data;

}

