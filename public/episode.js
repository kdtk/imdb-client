import Vue from 'vue';

import {ArrayToXml} from './utils';

import episode_template from "../views/components/episode.pug";

const EpisodeComponent = Vue.component('Episode', {
  template: episode_template(),

  props: ['episode', 'index', 'season'],

  data: function() {
    return {

      correct: false,

      isOpen: false,

      loaded_video_links: [],

      videoLinks: []
    }
  },

  created() {

    this.OriginalEpisode = Object.assign({}, this.episode);

    this.$on('set-videos', ( {links} ) => {
      this.videoLinks.splice(0, this.videoLinks.length, ...links );
    });
  },

  mounted() {

  },

  watch: {
    isOpen: {
      immediate: true,
      handler: function(newValue) {
        if ( !newValue ) {
          // checkdata
          this.correct = this.checkData([]) === true;
        }
      }
    }
  },

  computed: {

    number() {
      return this.episode.Number < 10 ? `0${this.episode.Number}` : this.episode.Number;
    },

    xml_generated() {
      return [{
        name: `${this.number} - ${this.episode.Title}`,
        color: 'violet',
        link: this.videoLinks.filter(link => !!link.link.trim() && !link.hidden).map(link => link.link.trim()),
        blink: this.videoLinks.filter(link => !!link.link.trim() && link.hidden).map(link => link.link.trim())
      }]
    }
  },

  methods: {

    toggleOpenEpisode() {
      this.isOpen = !this.isOpen;
    },

    resetField(field) {
      this.episode[ field ] = this.OriginalEpisode[ field ];
    },


    fillData(data) {
      this.episode.Title = data.name.substring( `${this.number} - `.length );
      this.loaded_video_links = data.links.slice(0)
    },


    checkData(links_to_check) {

      if ( this.episode.Title.trim().length <= 0 ) {
        // alert('Attenzione: devi specificare il Titolo');
        // return false;
        return `Ep${this.episode.Number} - Attenzione: devi specificare il Titolo`;
      }
      // controllo che ci sia almeno un link video
      if ( this.videoLinks.length <= 0 ) {
        // alert('Attenzione: specificare almeno un link video');
        // return false;
        return `Ep${this.episode.Number} - Attenzione: specificare almeno un link video`;
      } else {
        let vl = this.videoLinks.filter( link => !!link.link.trim() );
        if ( vl.length <= 0 ) {
          return `Ep${this.episode.Number} - Attenzione: occorre specificare almeno un link video valido`;
        }
        vl = this.videoLinks.filter( link => {
          let l = link.link.trim().toLowerCase();
          if ( l && l.indexOf('http') !== 0 ) {
            return true;
          }
          return false;
        });
        if ( vl.length > 0 ) {
          // alert('Attenzione: uno o più link dei video non sono url validi (http o https)')
          // return false;
          return `Ep${this.episode.Number} - Attenzione: uno o più link dei video non sono url validi (http o https)`;
        }
      }

      links_to_check.splice(links_to_check.length, 0, ...this.videoLinks.slice(0) );

      return true;
    },
    getXmlString() {
      return ArrayToXml( this.xml_generated, 'episode', `s${this.season.Number}e${this.episode.Number}`);
    }
  }
});
