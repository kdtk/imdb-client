function ArrayToXml(data, type, attr, tmdb) {

  let xmlStr = [];

  for ( let [index, datum] of data.entries() ) {
    if ( ! datum ) continue;
    // for ( let datum of season ) {
      // if ( ! datum ) continue;
      let block = [];

      let first_item_attrs = [];

      if ( attr ) {
        first_item_attrs.push(`t="${attr}"`);
      }

      if ( tmdb && index == 0 ) {
        first_item_attrs.push(`tmdb="${tmdb}"`);
      }

      block.push(`    <item ${first_item_attrs.join(' ')}>`);

      let entries = Object.entries( datum );

      for ( let [key,value] of entries ) {

        if ( type == 'movie' ) {
          // skip keys
          if ( key == 'h' && !value ) {
            // skip collection if empty
            continue;
          }
        }

        let str = [];

        if ( Array.isArray(value) ) {
          for ( let v of value ) {
            v = fixText(v);
            str.push( `      <${key}>${v}</${key}>` );
          }
        } else {
          value = fixText(value);
          str.push(`      <${key}>${value}</${key}>`);

        }

        block = block.concat( str );

      }


      block.push('    </item>');
      xmlStr.push( block );
    // }
  }

  return xmlStr.map(b => b.join('\n') ).join('\n');
}


function fixText(txt) {
  return txt.replace(/\n/g, ' ');
}


export {ArrayToXml};
