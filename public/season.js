import Vue from 'vue';

import {ArrayToXml} from './utils';

import './episode.js'

import season_template from "../views/components/season.pug";

const SeasonComponent = Vue.component('Season', {
  template: season_template(),

  props: ['season', 'index', 'card', 'isFirst'],

  data: function() {
    return {
      year: '',
      description: '',
      posterImage: '',
      backdropImage: '',

      isOpen: false,
      loadedPoster: '',
      loadedBackdrop: '',

      addDirectorsToXml: false,
      addDescriptionToXml: false,

      addImagesToXml: false
    }
  },

  created() {
    this.OriginalSeason = Object.assign({}, this.season);
    this.year = this.season.AirDate.substring(0, 4);
    this.description = this.season.Description;

    this.directorsStr = this.season.Directors.join(', ');


    this.$on('set-image', (obj) => {
      // setting posters and backdrops
      if ( obj.type.toLowerCase() == 'poster' ) {
        this.posterImage = obj.url;
      } else if ( obj.type.toLowerCase() == 'backdrop' ) {
        this.backdropImage = obj.url;
      }
    });

  },

  mounted() {

  },

  watch: {
  },


  computed: {
    googleDescrLink() {
      return 'https://www.google.com/search?q=' + `"${this.card.Title}"+"${this.season.Title}"+${this.year}+trama`;
    },
    descriptionCounter() {
      return this.description.length;
    },

    xml_generated() {

      if ( ! this.season._included ) {
        return [];
      }

      let data = {
        name: `${this.card.Title} - ${this.season.Title}`,
        color: `pink`
      };

      if ( this.isFirst ) {
        data.icon = `${this.$parent.posterImage}`;
        data.fanart = `${this.$parent.backdropImage}`;
      }
      if ( this.addImagesToXml ) {
        data.icon = `${this.posterImage}`;
        data.fanart = `${this.backdropImage}`;
      }

      data.year = this.year;

      if ( this.addDirectorsToXml ) {
        data.director = `${this.directorsStr}`;
      }

      if ( this.isFirst ) {
        // add genre (according to 'ordering')
        data.rating = this.$parent.rating;
        data.genre = `${this.card.GenresStr}`;
      }

      if ( this.addDescriptionToXml ) {
        data.description = `${this.description}`;
      }

      // First season workaround
      if ( this.isFirst ) {

        if ( !data.description ) {
          data.description = this.card.Description;
        }

        if ( !data.cast && this.$parent.addCastToXml ) {
          data.cast = `${this.card.CastStr}`;
        }

      }

      return [data];

    }
  },

  methods: {

    toggleOpenSeason() {
      this.isOpen = !this.isOpen;
    },

    resetField(field) {
      if ( field == 'Description' ) {
        this.description = this.season.Description || this.card.Description;
      } else if ( field == 'year') {
        this.year = this.season.AirDate.substring(0, 4);
      } else if ( field == 'directors' ) {
        this.directorsStr = this.season.Directors.join(', ');
      } else {
        this.season[ field ] = this.OriginalSeason[ field ];
      }

    },
    toggleOpenEpisode(index) {
      this.opened_episodes.splice( index , 1 , !this.opened_episodes[ index ] );
    },


    // fill data after Drag'n'Drop
    fillData(data) {
      this.season.Title = data.name.substring( `${this.card.Title} - `.length );
      this.year = data.year;

      this.description = data.description;
      if ( data.description ) {
        this.addDescriptionToXml = true;
      }

      this.directorsStr = data.director;
      if ( data.director ) {
        this.addDirectorsToXml = true;
      }

      // this.card.GenresStr = data.genre;

      this.loadedPoster = data.icon;
      this.loadedBackdrop = data.fanart;

      if ( data.icon && data.fanart ) {
        this.addImagesToXml = true;
      }
    },

    checkData(links_to_check) {

      if ( ! this.season._included ) {
        // Season is not included into xml
        return true;
      }

      if ( this.season.Title.trim().length <= 0 ) {
        return `S${this.season.Number} - Attenzione: specificare il titolo`;
      }

      if ( this.year.trim().length <= 0 ) {
        return `S${this.season.Number} - Attenzione: specificare l\'anno`;
      }

      if ( this.addDescriptionToXml ) {
        if ( this.description.trim().length <= 0 ) {
          return `S${this.season.Number} - Attenzione: specificare la descrizione`;
        } else if ( this.description.trim().length > 310 ) {
          return `S${this.season.Number} - Attenzione la descrizione supera il limite di caratteri consentito: 310`;
        }
      }


      if ( this.addDirectorsToXml ) {
        if ( this.directorsStr.trim().length <= 0 ) {
          return `S${this.season.Number} - Attenzione: devi specificare un Regista`;
        }
      }

      if ( this.addImagesToXml ) {
        // check for ICON
        if ( this.posterImage.trim().indexOf('.tmdb.') <= 0 ) {
          // alert(`Attenzione: l'immagine di Poster deve essere presa da TMDB`);
          // return false;
          return `S${this.season.Number} - Attenzione: l'immagine di Poster deve essere presa da TMDB`;
        }

        // check for BACKDROP
        if ( this.backdropImage.trim().indexOf('.tmdb.') <= 0 ) {
          // alert(`Attenzione: l'immagine di Poster deve essere presa da TMDB`);
          // return false;
          return `S${this.season.Number} - Attenzione: l'immagine di Fanart deve essere presa da TMDB`;
        }
      }

      for ( let [index,comp] of this.$children.entries() ) {
        if ( ! comp.checkData ) continue;
        let res = comp.checkData(links_to_check);
        if ( res === true ) continue;
        if ( res === false ) return false;
        if ( typeof res == 'string' ) {
          return `S${this.season.Number} ${res}`;
        }
      }

      return true;
    },

    getXmlString() {

      if ( ! this.season._included ) {
        // Season is not included into xml
        return;
      }

      let xml = [ ArrayToXml(this.xml_generated, 'season', `s${this.season.Number}`, this.$parent.card.Id ) ];
      for ( let comp of this.$children ) {
        if ( comp.getXmlString ) {
          xml.push( comp.getXmlString() );
        }
      }
      return xml.join('\n');
    }
  }
});
