import Vue from 'vue'


import './xmlbuilder.js'
import './movie.js'
import './tvserie.js'
import './image.js'
import './video.js'


const UploadBtn = document.querySelector('#upload-btn')
const DownloadBtn = document.querySelector('#download-btn')


function ajax(method, url, data) {
  return new Promise( (res, rej) => {
    let xmlHttp = new XMLHttpRequest(); // creates 'ajax' object
    xmlHttp.onreadystatechange = function(){
      if ( xmlHttp.readyState === 4 ) {
        if ( xmlHttp.status >= 200 && xmlHttp.status < 300 ) {
          res({status: xmlHttp.status, body: xmlHttp.responseText});
        } else {
          rej({status: xmlHttp.status, body: xmlHttp.responseText});
        }
      }
    }
    xmlHttp.open(method.toUpperCase(), url); //set method and address
    xmlHttp.send();
  });
}

window.EventEmitter = new Vue();

Card.Title = Card.Title.replace( /(\s)?\:(\s)?/, ' - ');

Card.is4K = false;
Card.GenresStr = Card.Genres.join(', ');
Card.DirectorsStr = Card.Directors.join(', ');
Card.CastStr = Card.Cast.join(', ');


if ( Card.tvshow ) {
  let to_add = [];

  for ( let i = Card.Seasons.length - 1; i >= 0; i--) {
    let s = Card.Seasons[ i ];
    s._included = false;
    if ( s.Number == 0 ) {
      // Specials
      to_add.push( s );
      Card.Seasons.splice( i, 1 );
    }
  }

  Card.Seasons.splice(Card.Seasons.length, 0, ...to_add);

}


const OriginalCard = JSON.parse(JSON.stringify(Card));

const VM = new Vue({
  el: '#main',
  data: {
    card: JSON.parse(JSON.stringify(Card)),
    showImdbData: false,
    original_card: OriginalCard
  },

  created() {

    UploadBtn.addEventListener('click', () => {
      for (let comp of this.$children) {
        let res = comp.checkData();
        if ( typeof res == 'string' ) {
          alert(res);
          return;
        } else if ( res === false ) {
          return;
        }
      }
      let resp = prompt('Inserire il nome del file (senza estensione):', `${this.card.Title}${this.card.is4K ? ' [HD-4K]' : ''}`);
      if ( !resp ) {
        return;
      }
      let xml_string = this.getAllXmlData();
      this.uploadXml( resp, xml_string );
    })
    DownloadBtn.addEventListener('click', () => {
      for (let comp of this.$children) {
        let res = comp.checkData();
        if ( typeof res == 'string' ) {
          alert(res);
          return;
        } else if ( res === false ) {
          return;
        }
      }
      let resp = prompt('Inserire il nome del file (senza estensione):', `${this.card.Title}${this.card.is4K ? ' [HD-4K]' : ''}`);
      if ( !resp ) {
        return;
      }
      let xml_string = this.getAllXmlData();
      this.downloadXml( resp, xml_string );
    })

    window.EventEmitter.$on('open-images', ({type}) => {
      this.exploreImages(type);
    })

    window.EventEmitter.$on('reset-field', ({field}) => {
      this.card[ field ] = OriginalCard[ field ];
    })


  },

  computed: {

    chars_count() {
      let count = 0;
      return count;
    },

    imdbUrl() {
      return `https://www.imdb.com/title/${this.card.ImdbId}`;
    },
    tmdbUrl() {
      return `https://www.themoviedb.org/${this.card.movie ? 'movie' : 'tv'}/${this.card.Id}-${this.card.OriginalTitle.replace(/\s/gi,'-').toLowerCase()}`;
    }

  },

  watch: {
  },

  methods: {

    getAllXmlData() {
      let xml = [
        `<data>`,
        `  <type name="channels">`
      ];
      for ( let comp of this.$children ) {
        if ( comp.getXmlString ) {
          let str = comp.getXmlString();
          if ( str !== undefined ) {
            xml.push( str );
          }
        }
      }

      xml = xml.concat([
        `  </type>`,
        `</data>`
      ]);

      return xml.join('\n');
    },

    showImdbDetails() {
      this.showImdbData = true;
    },
    hideImdbDetails() {
      this.showImdbData = false;
    },


    exploreImages(type) {
      window.open(`${this.tmdbUrl}/images/${type.toLowerCase()}s`)
    },

    uploadXml(filename, xml_string) {

      ajax('head', `uploadxml?name=${filename}`).then( (obj) => {
        uploadFile.call(this, false);
      }, (obj) => {
        if ( obj.status === 409 ) {
          uploadFile.call(this, true);
        } else {
          alert( obj.body );
        }
      });

      function uploadFile(ask) {
        let overwrite = false;
        if ( ask ) {
          let confirmation = confirm(`Il file '${filename}.xml esiste già. Sovrascriverlo?`);
          if ( !confirmation ) {
            return;
          } else {
            overwrite = true;
          }
        }

        let div = document.createElement('div');
        div.innerHTML = `
          <iframe name="temp_upload"></iframe>
          <form action="uploadxml" target="temp_upload" enctype="multipart/form-data" method="post">
            <input type="hidden" name="name" value="${filename}" />
            <textarea name="xml">${xml_string}</textarea>
            ${overwrite ? '<input type="hidden" name="force" value="1" />' : ''}
          </form>`;
        document.body.appendChild( div );

        let form = div.querySelector('form');

        form.submit();

        // document.body.removeChild(div);
        let iframe = div.querySelector('iframe');
        iframe.onload = () => {
          let _doc = iframe.contentDocument;
          let _body = _doc.body || _doc.querySelector('body');
          let _text = _body.textContent;
          alert( _text );
          document.body.removeChild(div);
          console.info('upload completed!')
        };
        iframe.onerror = () => {
          alert( `Errore durante l'upload\nControllare sul server se il file è stato scritto correttamente` );
          document.body.removeChild(div);
          console.warn('upload with error!')
        };
      }


    },


    downloadXml(filename, xmltext) {

      filename = `${filename}.xml`;
      let pom = document.createElement('a');
      let bb = new Blob([xmltext], {type: 'text/plain'});

      pom.setAttribute('href', window.URL.createObjectURL(bb) );
      pom.setAttribute('download', filename);

      pom.dataset.downloadurl = ['text/plain', pom.download, pom.href].join(':');


      document.body.appendChild( pom );
      pom.click();
      document.body.removeChild( pom );
    }


  }

});


window.VM = VM;
