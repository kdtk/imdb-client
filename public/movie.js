import Vue from 'vue';

import {ArrayToXml} from './utils';

import movie_template from "../views/components/movie.pug";

const MovieComponent = Vue.component('Movie', {

  template: movie_template(),

  props: ['card'],

  data: function() {
    return {
      showVote: false,

      rating: '',

      collection: '',
      posterImage: '',
      backdropImage: '',

      addCastToXml: true,

      videoLinks: [],

      loaded_video_links: [],

      loaded_poster: '',
      loaded_fanart: ''
    }
  },


  created() {

    window.MovieC = this;

    this.rating = this.card.ImdbData.rating;

    this.$on('set-image', (obj) => {
      // setting posters and backdrops
      if ( obj.type.toLowerCase() == 'poster' ) {
        this.posterImage = obj.url;
      } else if ( obj.type.toLowerCase() == 'backdrop' ) {
        this.backdropImage = obj.url;
      }
    });

    this.$on('set-videos', ( {links} ) => {
      this.videoLinks.splice(0, this.videoLinks.length, ...links );
    });

  },

  mounted() {
    DragDrop();
  },


  watch: {
  },


  computed: {

    googleDescrLink() {
      return 'https://www.google.com/search?q=' + `"${encodeURIComponent(this.card.Title)}"+${this.card.DateRange}+trama`;
    },

    castCounter() {
      return this.card.CastStr.split(',').length;
    },

    descriptionCounter() {
      return this.card.Description.length;
    },

    xml_generated() {

      let data = {
        name: `Trailer & Info: ${this.card.Title}`,
        color: `pink`,
        h: `${this.collection}`,
        icon: `${this.posterImage}`,
        fanart: `${this.backdropImage}`,
        year: `${this.card.DateRange}`,
        rating: String(parseFloat( this.rating ).toFixed(1)),
        genre: `${this.card.GenresStr}`,
        director: `${this.card.DirectorsStr}`
      };

      if ( this.addCastToXml ) {
        data.cast = `${this.card.CastStr}`;
      }

      data.description = `${this.showVote ? 'IMDB ' + this.card.ImdbData.rating + ' - ' : ''}${this.card.Description}`;
      data.link = [
        `plugin://plugin.video.youtube/play/?video_id=${this.card.YT_trailer}`,
        `plugin://script.extendedinfo/?info=extendedinfo&&id=${this.card.Id}`
      ];

      let res = [
        data,
        {
          name: `${this.card.Title}`,
          color: `violet`,
          link: this.videoLinks.filter(link => !!link.link.trim() && !link.hidden).map(link => link.link.trim()),
          blink: this.videoLinks.filter(link => !!link.link.trim() && link.hidden).map(link => link.link.trim())
        }
      ];

      return res;

    }
  },


  methods: {

    getXmlString() {
      return ArrayToXml(this.xml_generated, 'movie', null, this.card.Id);
    },

    cutCast() {
      let cs = this.card.CastStr.split(',');
      cs = cs.slice(0, 5);
      this.card.CastStr = cs.map( c => c.trim() ).join(', ');
    },


    resetField(field) {
      if ( field === 'rating') {
        this.rating = this.card.ImdbData.rating;
        return;
      }
      window.EventEmitter.$emit('reset-field', {field});
    },

    viewTrailer() {
      window.open(`https://www.youtube.com/watch?v=${this.card.YT_trailer}`);
    },


    checkData() {

      if ( this.card.Title.trim().length <= 0 ) {
        // alert('Attenzione: devi specificare il Titolo');
        // return false;
        return 'Attenzione: devi specificare il Titolo';
      }

      if ( this.card.GenresStr.trim().length <= 0 ) {
        // alert('Attenzione: devi specificare il Genere');
        // return false;
        return 'Attenzione: devi specificare il Genere';
      }

      if ( `${this.card.DateRange}`.trim().length <= 0 ) {
        // alert('Attenzione: devi specificare l\'Anno');
        // return false;
        return 'Attenzione: devi specificare l\'Anno'
      }

      if ( this.card.DirectorsStr.trim().length <= 0 ) {
        // alert('Attenzione: devi specificare un Regista');
        // return false;
        return 'Attenzione: devi specificare un Regista';
      }


      if ( this.card.Description.trim().length > 310 ) {
        // alert('Attenzione la descrizione supera il limite di caratteri consentito: 310');
        // return false;
        return 'Attenzione la descrizione supera il limite di caratteri consentito: 310';
      }

      // add genre (according to 'ordering')
      let rat = parseInt(this.rating, 10);
      if ( rat <= 0 ) {
        alert(`Attenzione: specificare il rating corretto`);
        return false;
      }

      // controllo che ci sia la locandina
      if ( this.posterImage.trim().indexOf('.tmdb.') <= 0 ) {
        // alert(`Attenzione: l'immagine di Poster deve essere presa da TMDB`);
        // return false;
        return `Attenzione: l'immagine di Poster deve essere presa da TMDB`;
      } else if ( this.posterImage.trim().indexOf('/original/') > -1 ) {
        let _check_img = confirm(`Sembra che l'immagine POSTER punti a "original". Continuare?`);
        if ( !_check_img ) return false;
      }

      // controllo che ci sia la fanart
      if ( this.backdropImage.trim().indexOf('.tmdb.') <= 0 ) {
        // alert(`Attenzione: l'immagine di Fanart deve essere presa da TMDB`);
        // return false;
        return `Attenzione: l'immagine di Fanart deve essere presa da TMDB`;
      } else if ( this.backdropImage.trim().indexOf('/original/') > -1 ) {
        let _check_img = confirm(`Sembra che l'immagine FANART punti a "original". Continuare?`);
        if ( !_check_img ) return false;
      }

      // controllo che non ci siano più di 7 attori
      if ( this.addCastToXml ) {
        if ( this.castCounter <= 0 || this.card.CastStr.trim().length <= 0 ) {
          return 'Attenzione: il Cast risulta vuoto';
        } else {
          if ( this.castCounter > 5 ) {
            if ( this.castCounter > 7 ) {
              // alert('Attenzione: sono stati riportati troppi attori nel Cast!')
              // return false;
              return 'Attenzione: sono stati riportati troppi attori nel Cast!';
            } else {
              if ( ! confirm(`Hai specificato ${this.castCounter} attori. Vuoi continuare ugualmente il salvataggio?`) ) {
                return false;
              }
            }
          }
        }
      }

      // Controllo che il campo youtube non sia vuoto
      if ( this.card.YT_trailer.trim().length <= 0 ) {
        // alert('Attenzione: non hai specificato un trailer valido');
        // return false;
        return 'Attenzione: non hai specificato un trailer valido';
      }

      // controllo che ci sia almeno un link video
      if ( this.videoLinks.length <= 0 ) {
        // alert('Attenzione: specificare almeno un link video');
        // return false;
        return 'Attenzione: specificare almeno un link video';
      } else {
        let vl = this.videoLinks.filter( link => !!link.link.trim() );
        if ( vl.length <= 0 ) {
          // alert('Attenzione: occorre specificare almeno un link video valido')
          // return false;
          return 'Attenzione: occorre specificare almeno un link video valido';
        }
        vl = this.videoLinks.filter( link => {
          let l = link.link.trim().toLowerCase();
          if ( l && l.indexOf('http') !== 0 ) {
            return true;
          }
          return false;
        });
        if ( vl.length > 0 ) {
          // alert('Attenzione: uno o più link dei video non sono url validi (http o https)')
          // return false;
          return 'Attenzione: uno o più link dei video non sono url validi (http o https)';
        }
      }

      if ( ! this.checkLinks( this.videoLinks.slice(0) ) ){
        return false;
      }

      return true;
    },


    checkLinks(xml) {

      xml = xml.filter( x => !!x.link.trim() ).flat();

      for ( let i = 0, l = xml.length - 1; i < l; i++ ) {
        let link1 = xml[i];
        for ( let j = i+1, z = xml.length; j < z; j++ ) {
          let link2 = xml[j];
          if ( link1 == link2 ) {
            alert('Attenzione: ci sono link ai video ripetuti');
            return false;
          }
        }
      }

      return true;
    }


  }

});


function DragDrop() {
  const MainContainer = document.documentElement; //querySelector("#main");
  MainContainer.addEventListener('drop', (ev) => {
    ev.preventDefault();
    MainContainer.classList.remove('dropping');
    // Use DataTransfer interface to access the file(s)
    if ( ev.dataTransfer && ev.dataTransfer.files ) {
      let file = ev.dataTransfer.files[0];
      if ( file ) {
        let filename = file.name.toLowerCase();
        let ext = filename.split('.').pop();
        if ( ext != 'xml' ) {
          alert(`Impossibile leggere il file ${ext}`);
          return;
        }
        let reader = new FileReader();
        reader.readAsText(file);
        reader.onload = (data) => {
          VM.card.is4K = filename.indexOf('[hd-4k]') > -1;
          loadXmlFromString( reader.result );
        };
        reader.onerror = (err) => {
          console.error(err);
          alert(`Errore: ${err}`);
        };
        return;
      }
    }
    alert('Nessun file valido trovato');
  });

  MainContainer.addEventListener('dragover', (ev) => {
    MainContainer.classList.add('dropping');
    ev.preventDefault();
  });
  MainContainer.addEventListener('dragleave', (ev) => {
    MainContainer.classList.remove('dropping');
    ev.preventDefault();
  });
}

function loadXmlFromString(str) {

  str = str.replace(/\&/gi, '&amp;');

  let dp = new DOMParser();
  let doc = dp.parseFromString(str, 'text/xml');

  if ( doc.documentElement.nodeName == "parsererror" ) {
    alert('Impossibile leggere il file');
    return;
  }

  let root = doc.documentElement;

  let items = Array.prototype.slice.call( root.querySelectorAll('type > item'), 0 );
  let mainInfo = items.shift();

  let titleEl = mainInfo.querySelector('name');
  let collectionEl = mainInfo.querySelector('h');
  let posterEl = mainInfo.querySelector('icon');
  let backdropEl = mainInfo.querySelector('fanart');
  let yearEl = mainInfo.querySelector('year');
  let ratingEl = mainInfo.querySelector('rating');
  let genreEl = mainInfo.querySelector('genre');
  let directorEl = mainInfo.querySelector('director');
  let castEl = mainInfo.querySelector('cast');
  let descriptionEl = mainInfo.querySelector('description');

  let links = Array.prototype.slice.call( mainInfo.querySelectorAll('link'), 0 );
  let trailerEl = null;
  for( let link of links ){
    if ( link.textContent.indexOf('youtube') > -1 ) {
      trailerEl = link;
    } else if ( link.textContent.indexOf('extendedinfo') > -1 ) {
      let matches_ids = link.textContent.match(/&id=(\d+)&?/);
      if ( matches_ids && matches_ids[1] ) {
        let ext_id = matches_ids[1];
        if ( VM.card.Id != ext_id ) {
          alert(`Sembra tu stia caricando un file non relativo a questo film: ${ext_id}`);
          return;
        }
      } else {
        alert('Il file xml sembra corrotto');
        return
      }
    }
  }

  let title = titleEl ? titleEl.textContent : '';
  let collection = collectionEl ? collectionEl.textContent : '';
  let poster = posterEl ? posterEl.textContent : '';
  let backdrop = backdropEl ? backdropEl.textContent : '';
  let year = yearEl ? yearEl.textContent : '';
  let rating = ratingEl ? ratingEl.textContent : '';
  let genre = genreEl ? genreEl.textContent : '';
  let director = directorEl ? directorEl.textContent : '';
  let cast = castEl ? castEl.textContent : '';
  let description = descriptionEl ? descriptionEl.textContent : '';
  let trailer = trailerEl ? trailerEl.textContent : '';

  title = title.substring('Trailer & Info: '.length);
  genre = genre.split(',').map( g => g.trim() ).filter(g => !!g);
  director = director.split(',').map( d => d.trim() ).filter(g => !!g);
  cast = cast.split(',').map( c => c.trim() ).filter(g => !!g);
  description = description.toLowerCase().trim().startsWith('imdb') ? description.substring( description.indexOf(' - ') + 3 ).trim() : description;

  trailer = trailer.split('=').pop();

  VM.card.Title = title.trim();
  MovieC.collection = collection.trim();
  VM.card.DateRange = year.trim();

  if ( rating ) {
    MovieC.rating = rating;
  }

  VM.card.Genres = genre;
  VM.card.GenresStr = genre.join(', ');
  VM.card.Directors = director;
  VM.card.DirectorsStr = director.join(', ');
  VM.card.Cast = cast;
  VM.card.CastStr = cast.join(', ');

  VM.card.Description = description.trim();

  VM.card.YT_trailer = `${trailer.trim()}`;


  MovieC.loaded_poster = poster.trim();
  MovieC.loaded_fanart = backdrop.trim();


  MovieC.loaded_video_links = [];

  // // video links
  // VM.videoLinks.splice(0, VM.videoLinks.length);
  for ( let item of items ) {
    let vlinks = Array.prototype.slice.call( item.querySelectorAll('link'), 0 );
    for ( let vlink of vlinks ) {
      let vurl = vlink.textContent || '';
      // let computeVData = extractVData( vurl.trim() );
      // VM.videoLinks.push( computeVData );
      MovieC.loaded_video_links.push( {hidden: false, link: vurl.trim()} );
    }
  }

  for ( let item of items ) {
    let vlinks = Array.prototype.slice.call( item.querySelectorAll('blink'), 0 );
    for ( let vlink of vlinks ) {
      let vurl = vlink.textContent || '';
      // let computeVData = extractVData( vurl.trim() );
      // VM.videoLinks.push( computeVData );
      MovieC.loaded_video_links.push( {hidden: true, link: vurl.trim()} );
    }
  }

}
