import Vue from 'vue';

import {ArrayToXml} from './utils';

import xmlviewer_template from "../views/components/xmlviewer.pug";

const EpisodeComponent = Vue.component('XmlViewer', {
  template: xmlviewer_template(),

  props: ['xml', 'type', 'skipxml', 'tmdb'],

  data: function() {
    return {
      xml_string: []
    }
  },

  created() {


  },

  mounted() {
  },

  watch: {
    xml: function(values) {
      this.xml_string = this.generateXml(values);
    },
    xml_string: {
      handler: function(values) {
        // if ( this.skipxml ) return;
        if ( this.$el ) {
          let xml_res_el = this.$el.querySelector('.xml-inner-viewer');
          if ( ! xml_res_el ) return;
          xml_res_el.innerHTML = '';
          // for( let xml of values ) {
            let code_el = document.createElement('code');
            code_el.classList.add('html');
            code_el.textContent = values;

            xml_res_el.appendChild( code_el );
          // }

          clearTimeout(this._timer);
          this._timer = setTimeout( () => {
              hljs.highlightBlock(code_el);
          }, 0);

        }
      }
    }
  },

  computed: {
    chars_count() {
      let count = 0;
      for ( let xml of this.xml_string ) {
        count += xml.length;
      }
      return count;
    }
  },

  methods: {
    generateXml(data) {

      return ArrayToXml(data, this.type, null, this.tmdb)
    }


  }
});
