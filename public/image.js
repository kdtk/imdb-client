import Vue from 'vue';

import image_template from "../views/components/image.pug";

const IconFanartComponent = Vue.component('IconFanart', {

  template: image_template(),

  props: ['card', 'refType', 'loadedImage'],

  data: function(){
    return {
      type: 'tmdb', // tmdb / personal,
      tmdb: {
        formats: [],
        res: '', // resolutions
        link: ''
      },
      personal: {
        res: '', // resolutions
        link: ''
      },
      computedRes: ''
    }
  },

  created() {

    if ( this.card[ this.computedRefType ] ) {

      let pk = Object.keys( this.card[ this.computedRefType ] );
      let pk_found = false;
      if ( this.refType == 'Poster' ) {
        pk = pk.filter( (k) => {
          if ( !pk_found ) {
            pk_found = k.indexOf('500') > -1
          }
          return pk_found;
        });
      }

      if ( this.refType == 'Backdrop' ) {
        pk = pk.slice( Math.floor(pk.length / 2) );
      }
      this.tmdb.formats = pk; // pk.slice( Math.floor(pk.length / 2) );
    } else {
      this.type = 'personal';
    }


    this.tmdb.res = this.tmdb.formats[0] || '';
  },


  computed: {

    label() {
      return this.refType == 'Poster' ? 'Poster | Icon' : 'Fanart';
    },

    computedRefType() {
      return `${this.refType}s`;
    },

    imageUrl() {
      let url = null;
      if ( this.type == 'tmdb' && this.tmdb.res) {
        url = this.card[ this.computedRefType ][ this.tmdb.res ];
      } else {
        url = this.personal.link;
      }

      // trigger image-url
      this.$parent.$emit('set-image', {type: this.refType, url});

      return url;
    }
  },

  watch: {
    imageUrl: function() {
      let imgurl = this.imageUrl;
      this.computedRes = '';
      if ( imgurl ) {
        let img = new Image();
        img.onload = () => {
          this.computedRes = `${img.width}x${img.height}`
          // document.body.removeChild(img);
        };
        img.onerror = () => {
          this.computedRes = '';
          // document.body.removeChild(img);
        };
        img.src = imgurl;
        // document.body.appendChild(img);
      }
    },

    loadedImage: {
      handler: function(newvalue) {
        let found = false;
        let pks = Object.keys( this.card[ this.computedRefType ] )
        for ( let pk of pks ) {
          let url = this.card[ this.computedRefType ][ pk ];
          if ( url == newvalue ) {
            this.tmdb.res = pk;
            this.type = 'tmdb';
            found = true;
            break;
          }
        }
        if ( !found ) {
          this.type = 'personal';
          this.personal.link = newvalue;
        }
      }
    }
  },

  methods: {
    exploreImages() {
      window.EventEmitter.$emit('open-images', {type: this.refType});
    }
  }

});
