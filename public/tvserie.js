import Vue from 'vue';

import './season.js';

import serie_template from "../views/components/serie.pug";

const UploadBtn = document.querySelector('#upload-btn')
const DownloadBtn = document.querySelector('#download-btn')


const REGEXP = /s(\d+)e?(\d+)?/i;

const SerieComponent = Vue.component('Serietv', {
  template: serie_template(),

  props: ['card'],

  data: function() {
    return {
      opened_seasons: [],

      xml_seasons: [],

      rating: '',

      addCastToXml: true,

      posterImage: '',
      backdropImage: '',
      loadedPoster: '',
      loadedBackdrop: ''
    }
  },

  created() {
    window.SerieC = this;

    this.OriginalSerie = Object.assign({}, this.card);

    this.rating = String(this.card.ImdbData ? this.card.ImdbData.rating : '0');

    this.opened_seasons.splice(0, 0, ...Array(this.card.Seasons.length).fill(false) );

    this.$on('set-image', (obj) => {
      // setting posters and backdrops
      if ( obj.type.toLowerCase() == 'poster' ) {
        this.posterImage = obj.url;
      } else if ( obj.type.toLowerCase() == 'backdrop' ) {
        this.backdropImage = obj.url;
      }
    });

  },

  mounted() {
    DragDrop.call(this);
  },

  watch: {
  },


  computed: {
    googleDescrLink() {
      return 'https://www.google.com/search?q=' + `"${this.card.Title}"+trama`;
    },
    descriptionCounter() {
      return this.card.Description.length;
    },
    castCounter() {
      return this.card.CastStr.split(',').length;
    },
    firstIncludedSeason() {
      return this.card.Seasons.filter( s => s._included )[0];
    }
  },

  methods: {
    toggleOpenSeason(index) {
      this.opened_seasons.splice( index , 1 , !this.opened_seasons[ index ] );
    },

    cutCast() {
      let cs = this.card.CastStr.split(',');
      cs = cs.slice(0, 5);
      this.card.CastStr = cs.map( c => c.trim() ).join(', ');
    },

    resetField(field) {
      if ( field == 'rating') {
        this.rating = this.card.ImdbData ? this.card.ImdbData.rating : '0';
        return;
      }
      this.card[ field ] = this.OriginalSerie[ field ];
    },

    checkData() {

      if ( this.card.Title.trim().length <= 0 ) {
        alert('Attenzione: devi specificare il Titolo');
        return false;
      }
      if ( this.card.GenresStr.trim().length <= 0 ) {
        alert('Attenzione: devi specificare il Genere');
        return false;
      }

      // controllo che non ci siano più di 7 attori
      if ( this.addCastToXml ) {
        if ( this.castCounter <= 0 || this.card.CastStr.trim().length <= 0 ) {
          alert('Attenzione: il Cast risulta vuoto');
          return false;
        } else {
          if ( this.castCounter > 5 ) {
            if ( this.castCounter > 7 ) {
              alert('Attenzione: sono stati riportati troppi attori nel Cast!');
              return false;
            } else {
              if ( ! confirm(`Hai specificato ${this.castCounter} attori. Vuoi continuare ugualmente il salvataggio?`) ) {
                return false;
              }
            }
          }
        }
      }

      // add genre (according to 'ordering')
      let rat = parseInt(this.rating, 10);
      if ( rat <= 0 ) {
        alert(`Attenzione: specificare il rating corretto`);
        return false;
      }


      let links_to_check = [];

      for ( let [index, comp] of this.$children.entries() ) {
        if ( comp.checkData ) {
          let res = comp.checkData(links_to_check);

          if ( res === true ) continue;
          if ( res === false ) continue;
          if ( typeof res === 'string' ) {
            return res;
          }
        }
      }

      if ( ! this.checkLinks( links_to_check ) ) {
        return false;
      }

      return true;
    },

    getXmlString() {
      let xml = [];
      for ( let comp of this.$children ) {
        if ( comp.getXmlString ) {
          let res = comp.getXmlString();
          if ( res !== undefined ) {
            xml.push( res );
          }
        }
      }

      return xml.join('\n');
    },

    checkLinks(xml) {

      xml = xml.filter( x => !!x.link.trim() ).flat();

      for ( let i = 0, l = xml.length - 1; i < l; i++ ) {
        let link1 = xml[i];
        for ( let j = i+1, z = xml.length; j < z; j++ ) {
          let link2 = xml[j];
          if ( link1 == link2 ) {
            alert('Attenzione: ci sono link ai video ripetuti');
            return false;
          }
        }
      }

      return true;
    }
  }
});





function DragDrop() {
  const MainContainer = document.documentElement; //querySelector("#main");
  MainContainer.addEventListener('drop', (ev) => {
    ev.preventDefault();
    MainContainer.classList.remove('dropping');
    // Use DataTransfer interface to access the file(s)
    if ( ev.dataTransfer && ev.dataTransfer.files ) {
      let file = ev.dataTransfer.files[0];
      if ( file ) {
        let filename = file.name.toLowerCase();
        let ext = filename.split('.').pop();
        if ( ext != 'xml' ) {
          alert(`Impossibile leggere il file ${ext}`);
          return;
        }
        let reader = new FileReader();
        reader.readAsText(file);
        reader.onload = (data) => {
          VM.card.is4K = filename.indexOf('[hd-4k]') > -1;
          loadXmlFromString.call( this, reader.result );
        };
        reader.onerror = (err) => {
          console.error(err);
          alert(`Errore: ${err}`);
        };
        return;
      }
    }
    alert('Nessun file valido trovato');
  });

  MainContainer.addEventListener('dragover', (ev) => {
    MainContainer.classList.add('dropping');
    ev.preventDefault();
  });
  MainContainer.addEventListener('dragleave', (ev) => {
    MainContainer.classList.remove('dropping');
    ev.preventDefault();
  });
}



function loadXmlFromString(str) {

  str = str.replace(/\&/gi, '&amp;');

  let dp = new DOMParser();
  let doc = dp.parseFromString(str, 'text/xml');

  if ( doc.documentElement.nodeName == "parsererror" ) {
    alert('Impossibile leggere il file');
    return;
  }

  let root = doc.documentElement;

  let items = Array.prototype.slice.call( root.querySelectorAll('type > item'), 0 );

  for ( let [index, item] of items.entries() ) {
    let _ref_ = item.getAttribute('t') || '';


    if ( ! _ref_ ) {
      alert(
        [
        `Attenzione: stai cercando di caricare un xml in formato obsoleto (item ${index + 1})`,
        `Per caricare il file occorre che gli 'item' abbiano specificato l'attributo "t", indicando la stagione e l'episodio di riferimento.`,
        `Esempio:`,
        `<item t="s1">     nel caso di Stagione 1`,
        `<item t="s2e1"    nel caso di Episodio 1 della Stagione 2`
        ].join('\n')
      );
      return;
    }

    let matches = _ref_.match( REGEXP );
    if ( !matches || matches.length < 2 ) {
      alert( `Attenzione: "item ${index + 1}"  ha l'attributo "t" non valido: ${_ref_}`);
      return;
    }

    let _season_ = parseInt( matches[1], 10);
    let _episode_ = parseInt(matches[2], 10);

    let name_el = item.querySelector('name');
    let year_el = item.querySelector('year');
    let rating_el = item.querySelector('rating');
    let description_el = item.querySelector('description');
    let director_el = item.querySelector('director');
    let cast_el = item.querySelector('cast');
    let genre_el = item.querySelector('genre');
    let icon_el = item.querySelector('icon');
    let fanart_el = item.querySelector('fanart');

    if ( rating_el ) {
      let rating = rating_el.textContent;
      if ( rating && rating.trim() ) {
        SerieC.rating = rating.trim();
      }
    }


    let name = name_el ? name_el.textContent : '';
    let year = year_el ? year_el.textContent : '';
    let description = description_el ? description_el.textContent : '';
    let director = director_el ? director_el.textContent : '';
    let cast = cast_el ? cast_el.textContent : '';
    let genre = genre_el ? genre_el.textContent : '';
    let icon = icon_el ? icon_el.textContent : '';
    let fanart = fanart_el ? fanart_el.textContent : '';

    let links = Array.prototype.slice.call( item.querySelectorAll('link'), 0 );
    links = links.map( l => l.textContent ).map( l => l.trim() ).filter( l => !!l ).map( (l) => {return {hidden: false, link: l}});
    let blinks = Array.prototype.slice.call( item.querySelectorAll('blink'), 0 );
    blinks = blinks.map( l => l.textContent ).map( l => l.trim() ).filter( l => !!l ).map( (l) => {return {hidden: true, link: l}});

    links = links.concat( blinks );

    let stop = false;

    for ( let comp of this.$children ) {
      let componentTag = comp.$options._componentTag.toLowerCase();

      if ( componentTag == 'season' && comp.season.Number == _season_ ) {

        comp.season._included = true;

        if ( _episode_ !== undefined && !isNaN(_episode_) ) {
          for ( let comp_ep of comp.$children ) {
            let ep_componentTag = comp_ep.$options._componentTag.toLowerCase();
            if ( ep_componentTag == 'episode' && comp_ep.episode.Number == _episode_ ) {
              // fill episode
              comp_ep.fillData({
                name,
                links
              });
              stop = true;
              break;
            }
          }
        } else {
          // fill season

          if ( index === 0 ) {
            // getting data for first season: populate TvSerie general data
            this.card.GenreStr = genre;
            this.card.CastStr = cast;
            this.card.Description = description;
            this.loadedPoster = icon;
            this.loadedBackdrop = fanart;
          }

          comp.fillData({
            name,
            year,
            description,
            director,
            icon,
            fanart,
            links
          });
          break;
        }
      }

      if  ( stop ) {
        break;
      }

    }

  }

}

function getAllChildrenComps(main, types) {

  let result = [];
  for ( let comp of main.$children ) {

    if ( types ) {
      let componentTag = comp.$options._componentTag.toLowerCase();
      if ( types.indexOf( componentTag ) > -1 ) {
        result.push( comp );
      }
    }

    let subcomps = getAllChildrenComps(comp, types);
    result = result.concat( subcomps );

  }

  return result;

}
