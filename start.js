const Cluster = require('cluster');
const OS = require('os')


if ( Cluster.isMaster ) {
  const CPUs = OS.cpus();
  for (let cpu of CPUs ) {
    Cluster.fork();
  }

} else {
  require('./application');
}
