require('dotenv').config();
const Express = require('express');
const CORS = require('cors');
const Helmet = require('helmet');
const BodyParser = require('body-parser');
const Package = require('./package.json');
const BasicAuth = require('express-basic-auth');
const Multer = require("multer");
const FS = require('fs');
const Path = require('path');
const Uploader = Multer();

const Config = process.env;
Config.CONTEXT_PATH = Config.CONTEXT_PATH || '';

if ( Config.LOGO ) {
  Config.LOGO = `logo-${Config.LOGO}`;
} else {
  Config.LOGO = `logo`;
}

// require('./telegram_bot');

const Utils = require('./utils');
const Log = Utils.Log;


const App = Express();


App.set('trust proxy', 1);
App.use( Helmet() );
App.set('view engine', 'pug');
App.use( CORS() );
App.use( Express.static( `${__dirname}/public`) );

App.locals = Object.assign( App.locals || {}, Config, { NAME: 'Xml Tool Editor', Vers: Package.version } );

// App.use( Session({
//   saveUninitialized: false,
//   resave: false,
//   secret : 's3Cur3',
//   name : 'sid',
// }));

App.use( BodyParser.json({extended: true}) );
App.use( BodyParser.urlencoded({extended: true}) );

App.use( Express.static('node_modules/bootstrap/dist/css/') );


if ( Config.KODITEKA_USR ) {
  App.use( BasicAuth({challenge: true, users: { [Config.KODITEKA_USR]: Config.KODITEKA_PWD }}) );
}


App.get('/', (req, res, next) => {
  Log.info(`Rendering index`);
  res.render('results');
});



App.get('/info.:format?', (req, res, next) => {

  const terms = req.query.q;
  const format = req.params.format || 'html';

  Log.info(`Searching for ${terms} in ${format}`);

  if ( format == 'json' ) {
    res.set('content-type', 'application/json');
  } else {
    res.set('content-type', 'text/html');
  }

  if ( terms && terms.trim() ) {
    Utils.searchByTerms(terms).then( (results) => {

      Log.info(`Having results for ${results.results.length}`);

      if ( format == 'json' ) {
        res.status(200).end( JSON.stringify(results.results) );
      } else {
        res.render('results', {results: results.results, imgOptions: Utils.getImagesOpts() });
      }

    }, (err) => {
      Log.error(`Error ${err}`);
      res.status(422).end( JSON.stringify(err) );
    });
  } else {
    res.render('results');
  }

});


App.get('/info/:id.:format?', (req, res, next) => {

  const id = req.params.id;
  const type = req.query.type || 'movie';
  const format = req.params.format || 'html';
  const pretty = !!req.query.b;

  Log.info(`Getting details for ${id} in ${format}`);

  if ( format == 'json' ) {
    res.set('content-type', 'application/json');
  } else {
    res.set('content-type', 'text/html');
  }

  Utils.getInfo( id, type ).then( (card) => {

    Log.info(`Having result for ${id}: ${card.Title}`);

    if ( format == 'json' ) {
      res.status(200).end( JSON.stringify(pretty ? card.toPage() : card.data) );
    } else {
      res.render('details', {card} );
    }

  }, (err) => {
    Log.error(`Error ${err}`);
    res.status(422).end(`${err}`);
  });

});


App.get('/info/:id/edit', (req, res, next) => {

  const id = req.params.id;
  const type = req.query.type || 'movie';
  const format = req.params.format || 'html';

  Log.info(`Getting details for ${id} in ${format}`);

  res.set('content-type', 'text/html');

  Utils.getInfo( id, type ).then( (card) => {

    Log.info(`Having result for ${id}: ${card.Title}`);

    res.render('koditeka', {card} );

  }, (err) => {
    Log.error(`Error ${err.message}`);
    res.status(422).end(`${err.message}`);
  });

});


App.head('/info/:id/uploadxml', (req, res, next) => {
  let filename = req.query.name;
  if ( Config.UPLOAD_PATH ) {

    let path = Path.resolve(Config.UPLOAD_PATH);
    path = Path.join(path, `${filename}.xml`);

    if ( FS.existsSync(path) ) {
      res.status(409).end(`ERRORE: Il file '${filename}.xml' è già presente`);
    } else {
      res.status(202).end('');
    }
  } else {
    res.status(503).end('ERRORE: No upload path specified');
  }
});

App.post('/info/:id/uploadxml', Uploader.none(), (req, res, next) => {
  // req.body.xml
  let filename = req.body.name;
  if ( Config.UPLOAD_PATH ) {

    let path = Path.resolve(Config.UPLOAD_PATH);
    path = Path.join(path, `${filename}.xml`);

    if ( FS.existsSync(path) && !req.body.force) {
      res.status(409).end(`ERRORE: Il file '${filename}.xml' è già presente`);
    } else {
      FS.writeFileSync( path, req.body.xml, 'utf-8');
      res.status(201).end(`File creato correttamente nella cartella di test: '${filename}.xml'`);
    }
  } else {
    res.status(503).end('ERRORE: No upload path specified');
  }

});



App.listen(Config.PORT, () => {
  Log.info(`Start listening on port ${App.locals.NAME || "Xml Editor"}:${Config.PORT} `);
})
